package com.sapient.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.sapient.model.Trade;
import com.sapient.services.TradeService;

@RestController
public class AppController {

	@Autowired
	private TradeService tradeService;

	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}

	@RequestMapping(value = "/trade", method = RequestMethod.GET)
	public ResponseEntity<List<Trade>> getAlltrades() {
		List<Trade> tradeList = tradeService.getAllTrades();
		System.out.println("getting All Trades");
		if (tradeList.isEmpty()) {
			return new ResponseEntity<List<Trade>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<Trade>>(tradeList, HttpStatus.OK);

	}

	@RequestMapping(value = "/trade/{id}", method = RequestMethod.GET)
	public ResponseEntity<Trade> getTrade(@PathVariable("id") long id) {
		System.out.println("getting  Trade");
		Trade trade = tradeService.getTradeByID(id);
		if (trade == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(trade, HttpStatus.OK);

	}

	@RequestMapping(value = "/trade", method = RequestMethod.POST)
	public ResponseEntity<Void> createTrade(@RequestBody Trade trade, UriComponentsBuilder ucBuilder) {
		System.out.println("creating  Trade");
		if (tradeService.tradeExists(trade.getId())) {
			System.out.println("trade with id"+" " + trade.getId() + " Already exists ");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		tradeService.saveTrade(trade);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(trade.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/trade/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Trade> updateTrade(@PathVariable("id") long id, @RequestBody Trade trade) {
		System.out.println("updating  Trade");
		Trade currentTrade = tradeService.getTradeByID(id);
		if (currentTrade == null)

		{
			System.out.println("Trade not Found for updating");
			return new ResponseEntity<Trade>(HttpStatus.NOT_FOUND);
		}

		currentTrade.setUnderLying(trade.getUnderLying());
		tradeService.updateTrade(currentTrade);

		return new ResponseEntity<Trade>(trade, HttpStatus.OK);
	}

	@RequestMapping(value = "/trade/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Trade> deleteTrade(@PathVariable long id) {

		System.out.println("deleting  Trade");
		Trade trade = tradeService.getTradeByID(id);
		if (trade == null) {
			return new ResponseEntity<Trade>(HttpStatus.NOT_FOUND);

		}
		tradeService.delete(id);
		return new ResponseEntity<Trade>(HttpStatus.NO_CONTENT);

	}

	@RequestMapping(value = "/trade", method = RequestMethod.DELETE)
	public ResponseEntity<Trade> deleteAllTrades() {
		System.out.println("deleting all Trades");
		tradeService.deletAlltrade();
		return new ResponseEntity<Trade>(HttpStatus.NO_CONTENT);
	}



}
