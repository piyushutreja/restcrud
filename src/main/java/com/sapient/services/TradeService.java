package com.sapient.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sapient.dao.TradeDAO;
import com.sapient.model.Trade;

@Service
public class TradeService {

	@Autowired
	private TradeDAO tradeDAO;

	public List<Trade> getAllTrades() {

		return tradeDAO.getAllTrades();
	}

	public Trade getTradeByID(long id) {

		return tradeDAO.getTradeByID(id);

	}

	public boolean tradeExists(long id) {

		return tradeDAO.tradeExists(id);

	}

	public void saveTrade(Trade trade)  {
		tradeDAO.saveTrade(trade);

	}

	public void updateTrade(Trade currentTrade) {

		tradeDAO.updateTrade(currentTrade);

	}

	public void delete(long id) {
		tradeDAO.delete(id);

	}

	public void deletAlltrade() {
		tradeDAO.deletAlltrade();

	}

}
