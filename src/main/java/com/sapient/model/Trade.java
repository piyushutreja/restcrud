package com.sapient.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "trade")
public class Trade {

	@Id
	@GeneratedValue
	private long id;
	
	@Column(name = "underlying")
	@NotNull
	private String underLying;
	
	public Trade()	{
		
	}

	public String getUnderLying() {
		return underLying;
	}

	public void setUnderLying(String underLying) {
		this.underLying = underLying;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/*@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((underLying == null) ? 0 : underLying.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trade other = (Trade) obj;
		if (id != other.id)
			return false;
		if (underLying == null) {
			if (other.underLying != null)
				return false;
		} else if (!underLying.equals(other.underLying))
			return false;
		return true;
	}*/

	
	
}
