package com.sapient.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sapient.model.Trade;

@Transactional
@Repository
public class TradeDAOImpl implements TradeDAO {

	@PersistenceContext
	private EntityManager entityManager;

	
	@Override
	public List<Trade> getAllTrades() {
		
		@SuppressWarnings("unchecked")
		List<Trade> tradeList = entityManager.createQuery("FROM Trade").getResultList();
		return tradeList;
	}

	@Override
	public Trade getTradeByID(long id) {
		return entityManager.find(Trade.class, id);

	}

	@Override
	public boolean tradeExists(long id) {
		if (getTradeByID(id) == null) {
			return false;
		} else
			return true;

	}

	@Override
	public void saveTrade(Trade trade) {
		entityManager.persist(trade);

	}

	@Override
	public void updateTrade(Trade currentTrade) {
		Trade trade = getTradeByID(currentTrade.getId());
		trade.setUnderLying((currentTrade.getUnderLying()));
		entityManager.flush();

	}

	@Override
	public void delete(long id) {
		entityManager.remove(getTradeByID(id));

	}

	@Override
	public void deletAlltrade() {
		entityManager.createQuery("DELETE from Trade").executeUpdate();

	}

}
