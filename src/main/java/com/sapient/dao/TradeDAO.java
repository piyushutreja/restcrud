package com.sapient.dao;

import java.util.List;

import com.sapient.model.Trade;

public interface TradeDAO {
	public List<Trade> getAllTrades();

	public Trade getTradeByID(long id);

	public boolean tradeExists(long id);

	public void saveTrade(Trade trade);

	public void delete(long id);

	public void deletAlltrade();

	void updateTrade(Trade currentTrade);

}
