@GET
Feature: GET feature of service

  Scenario: Retrieve specific trades with id
    Given the system already has following trades:
      | underLying |
      | stock1     |
      | stock2     |
    When the client requests GET /trade/id with trade id "1"
    Then the api should retrive specific trades with below attributes:
      | underLying |
      | stock1     |
    Then delete all trades


  Scenario: Retrieve all the trades
    Given the system already has following trades:
      | underLying   |
      | sampleTrade1 |
      | sampleTrade2 |
    When the client requests GET /trade
    Then the api should retrive trades with below attributes:
      | underLying   |
      | sampleTrade1 |
      | sampleTrade2 |
    Then delete all trades