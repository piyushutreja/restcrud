@PUT
Feature: PUT feature of service

  Scenario: update specific Trade
    Given the system already has following trades:
      | underLying |
      | stock1     |
      | stock2     |
    When the client requests PUT /trade/id with id "1" and below body data:
      | underLying   |
      | updatedStock |
    Then the api should update specific trades and return code 200 OK
    Then delete all trades
