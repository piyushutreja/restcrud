@POST
Feature: Post feature of service

  Scenario: save new Trade
    When the client requests POST /trade with below body data:
      | underLying |
      | stock1     |
    Then the api should save specific trades and return code 201 Created
    Then delete all trades
