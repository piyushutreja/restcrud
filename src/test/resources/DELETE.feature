@DELETE
Feature: DELETE feature of service

  Scenario: deleting specific Trade
    Given the system already has following trades:
      | underLying |
      | stock1     |
      | stock2     |
    When the client requests DELETE /trade/id with id "1"
    Then the api should update specific trades and return code 204 NoContent
    Then delete all trades

  Scenario: delete all Trades 
    Given the system already has following trades:
      | underLying |
      | stock1     |
      | stock2     |
    When the client requests DELETE /trade 
    Then delete all trades
    And  return code 204 NoContent
