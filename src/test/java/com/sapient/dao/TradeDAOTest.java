package com.sapient.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;
import com.sapient.model.Trade;

@RunWith(SpringRunner.class)
@ComponentScan
@SpringBootTest
@AutoConfigureTestDatabase
@Transactional
public class TradeDAOTest {

	@Autowired
	private TradeDAO tradeDAO;

	@Autowired
	private EntityManager entityManager;

	@Test
	public void should_get_All_Trades() {

		List<Trade> tradeList = tradeDAO.getAllTrades();
		assertThat(tradeList != null);
		assertThat(tradeList.size() == 1);
	}

	@Test
	public void should_getTrade_ByID_Positive() {
		List<Trade> tradeList = tradeDAO.getAllTrades();
		Trade foundTrade = tradeDAO.getTradeByID(tradeList.get(0).getId());
		assertThat(foundTrade);

	}

	@Test(expected = NullPointerException.class)
	public void should_getTrade_ByID_Negative() {
		Trade foundTrade = tradeDAO.getTradeByID(10);
		assertTrue(foundTrade.getId() == 10);
		fail();

	}

	@Test
	public void check_if_tradeExists_Positive() {

		assertThat(tradeDAO.tradeExists(1));

	}

	@Test
	public void check_if_tradeExists_Negative() {

		assertFalse(tradeDAO.tradeExists(10));

	}

	@Test
	public void shoud_save_Trade_Postive() {
		Trade sampleTrade = new Trade();
		sampleTrade.setUnderLying("sampleTrade");
		tradeDAO.saveTrade(sampleTrade);
		List<Trade> tradeList = tradeDAO.getAllTrades();
		assertTrue(tradeList.size() == 2);
		assertTrue(tradeList.get(1).equals(sampleTrade));

	}

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void shoud_save_Trade_Negative() {
		Trade sampleTrade = null;
		tradeDAO.saveTrade(sampleTrade);
		tradeDAO.getAllTrades();
		fail();
	}

	@Test
	public void should_delete_trade_Postive() {

		List<Trade> tradeList = tradeDAO.getAllTrades();
		tradeDAO.delete(tradeList.get(0).getId());
		Trade trade = tradeDAO.getTradeByID(1);
		assertThat(trade == null);

	}

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void should_delete_trade_negative() {
		tradeDAO.delete(2);
		fail();
	}

	@Test
	public void should_delete_All_Trade() {
		tradeDAO.deletAlltrade();
		List<Trade> tradeList = tradeDAO.getAllTrades();
		assertTrue(tradeList.size() == 0);
	}

	@Test
	public void should_update_Trade_Positive() {
		List<Trade> tradeList = tradeDAO.getAllTrades();
		Trade sampleTrade = tradeList.get(0);
		sampleTrade.setUnderLying("sample2");
		tradeDAO.updateTrade(sampleTrade);
		List<Trade> tradeListUpdated = tradeDAO.getAllTrades();
		Trade updatedSampleTrade = tradeListUpdated.get(0);
		assertTrue(updatedSampleTrade.getUnderLying().equals(sampleTrade.getUnderLying()));

	}

	@Test(expected = NullPointerException.class)
	public void should_update_Trade_Negative() {
		Trade sampleTrade = null;
		sampleTrade.setUnderLying("sample2");
		tradeDAO.updateTrade(sampleTrade);
		fail();
	}

	@Before
	public void setup() {
		Trade trade1 = new Trade();
		trade1.setUnderLying("stock1");
		entityManager.persist(trade1);

	}

	@After
	public void tearDown() {
		entityManager.createQuery("DELETE from Trade").executeUpdate();

	}

}
