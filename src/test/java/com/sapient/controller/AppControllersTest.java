package com.sapient.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sapient.model.Trade;
import com.sapient.services.TradeService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureMockMvc
public class AppControllersTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private TradeService tradeService;

	@InjectMocks
	@Autowired
	private AppController appController;

	@Test
	public void should_get_All_Trades_Positive() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		Trade trade2 = new Trade();
		trade2.setId(2);
		trade2.setUnderLying("stock2");
		List<Trade> tradeList = Arrays.asList(trade1, trade2);
		when(tradeService.getAllTrades()).thenReturn(tradeList);
		mockMvc.perform(get("/trade")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].underLying", is("stock1"))).andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].underLying", is("stock2")));
		verify(tradeService, times(1)).getAllTrades();

	}

	@Test
	public void should_get_All_Trades_Negative() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		Trade trade2 = new Trade();
		trade2.setId(2);
		trade2.setUnderLying("stock2");
		List<Trade> tradeList = Collections.emptyList();
		when(tradeService.getAllTrades()).thenReturn(tradeList);
		mockMvc.perform(get("/trade")).andDo(print()).andExpect(status().isNoContent());
		verify(tradeService, times(1)).getAllTrades();

	}

	@Test
	public void should_get_trade_by_Id_Positive() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		when(tradeService.getTradeByID(1)).thenReturn(trade1);
		mockMvc.perform(get("/trade/{id}", trade1.getId())).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.underLying", is("stock1")));
		verify(tradeService, times(1)).getTradeByID(1);

	}

	@Test
	public void should_get_trade_by_Id_Negative() throws Exception {
		Trade trade1 = null;

		when(tradeService.getTradeByID(1)).thenReturn(trade1);
		mockMvc.perform(get("/trade/{id}", 1)).andDo(print()).andExpect(status().isNotFound());
		verify(tradeService, times(1)).getTradeByID(1);

	}

	@Test
	public void should_create_trade_Positive() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		doNothing().when(tradeService).saveTrade(trade1);
		mockMvc.perform(post("/trade").contentType(MediaType.APPLICATION_JSON).content(asJsonString(trade1)))
				.andExpect(status().isCreated());

	}

	@Test
	public void should_create_trade_Negative() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		when(tradeService.tradeExists(trade1.getId())).thenReturn(true);
		mockMvc.perform(post("/trade").contentType(MediaType.APPLICATION_JSON).content(asJsonString(trade1)))
				.andExpect(status().isConflict());

	}

	@Test
	public void should_update_trade_Positive() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		when(tradeService.getTradeByID(trade1.getId())).thenReturn(trade1);
		doNothing().when(tradeService).updateTrade(trade1);
		mockMvc.perform(put("/trade/{id}", trade1.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trade1))).andExpect(status().isOk());
		verify(tradeService, times(1)).getTradeByID(trade1.getId());
		verify(tradeService, times(1)).updateTrade(trade1);
	}

	@Test
	public void should_update_trade_Negative() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		when(tradeService.getTradeByID(trade1.getId())).thenReturn(null);
		mockMvc.perform(put("/trade/{id}", trade1.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trade1))).andExpect(status().isNotFound());
		verify(tradeService, times(1)).getTradeByID(trade1.getId());

	}

	@Test
	public void should_delete_trade_byID_Positive() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		when(tradeService.getTradeByID(trade1.getId())).thenReturn(trade1);
		doNothing().when(tradeService).delete(trade1.getId());
		mockMvc.perform(delete("/trade/{id}", trade1.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trade1))).andExpect(status().isNoContent());

	}

	@Test
	public void should_delete_trade_byID_Negative() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		when(tradeService.getTradeByID(trade1.getId())).thenReturn(null);
		doNothing().when(tradeService).delete(trade1.getId());
		mockMvc.perform(delete("/trade/{id}", trade1.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trade1))).andExpect(status().isNotFound());

	}

	@Test
	public void should_delete_all_trade_Positive() throws Exception {
		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setUnderLying("stock1");
		when(tradeService.getTradeByID(trade1.getId())).thenReturn(trade1);
		doNothing().when(tradeService).deletAlltrade();
		mockMvc.perform(delete("/trade")).andExpect(status().isNoContent());

	}

	private static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
