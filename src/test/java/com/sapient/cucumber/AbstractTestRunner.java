package com.sapient.cucumber;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import com.sapient.DemoApplication;
import com.sapient.controller.AppController;
import com.sapient.services.TradeService;

@ContextConfiguration(classes = DemoApplication.class, loader = SpringBootContextLoader.class)
@WebAppConfiguration
@AutoConfigureMockMvc
@TestExecutionListeners({ DirtiesContextTestExecutionListener.class })

public abstract class AbstractTestRunner {

	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	protected AppController appController;

	@Autowired
	protected TradeService tradeService;

	@Autowired
	protected EntityManager entityManager;

}
