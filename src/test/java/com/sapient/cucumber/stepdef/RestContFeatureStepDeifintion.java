package com.sapient.cucumber.stepdef;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.ResultActions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sapient.cucumber.AbstractTestRunner;
import com.sapient.model.Trade;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;;

@DirtiesContext
public class RestContFeatureStepDeifintion extends AbstractTestRunner {

	ResultActions resultActions;

	@Given("^the system already has following trades:$")
	public void the_system_already_has_following_trades(DataTable arg1) throws Throwable {

		List<String> data = inputFormatterUtil(arg1);
		for (String string : data) {
			Trade trade = new Trade();
			trade.setUnderLying(string);
			tradeService.saveTrade(trade);
		}

	}

	@When("^the client requests GET /trade$")
	public void the_client_requests_GET_trade() throws Throwable {

		resultActions = mockMvc.perform(get("/trade"));

	}

	@Then("^the api should retrive trades with below attributes:$")
	public void the_api_should_retrive_trades_with_below_attributes(DataTable arg1) throws Throwable {

		List<String> data = inputFormatterUtil(arg1);

		resultActions.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].underLying", is(data.get(0))))
				.andExpect(jsonPath("$[1].underLying", is(data.get(1))));
	}

	@When("^the client requests GET /trade/id with trade id \"([^\"]*)\"$")
	public void the_client_requests_GET_trade_with_trade_id(String arg1) throws Throwable {

		resultActions = mockMvc.perform(get("/trade/{id}", arg1));

	}

	@Then("^the api should retrive specific trades with below attributes:$")
	public void the_api_should_retrive_specific_trades_with_below_attributes(DataTable arg1) throws Throwable {
		List<String> data = inputFormatterUtil(arg1);
		resultActions.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.underLying", is(data.get(0))));
	}

	@Then("^delete all trades$")
	public void delete_all_trades() throws Throwable {
		tradeService.deletAlltrade();
	}

	@When("^the client requests POST /trade with below body data:$")
	public void the_client_requests_POST_trade_with_below_body_data(DataTable arg1) throws Throwable {
		List<String> data = inputFormatterUtil(arg1);
		Trade trade = new Trade();
		trade.setUnderLying(data.get(0));

		resultActions = mockMvc
				.perform(post("/trade").contentType(MediaType.APPLICATION_JSON).content(asJsonString(trade)));

	}

	@Then("^the api should save specific trades and return code (\\d+) Created$")
	public void the_api_should_save_specific_trades_and_return_code_Created(int arg1) throws Throwable {

		resultActions.andExpect(status().isCreated());

	}

	@When("^the client requests PUT /trade/id with id \"([^\"]*)\" and below body data:$")
	public void the_client_requests_PUT_trade_id_with_id_and_below_body_data(String arg1, DataTable arg2)
			throws Throwable {
		List<String> data = inputFormatterUtil(arg2);
		Trade trade = new Trade();
		trade.setUnderLying(data.get(0));
		resultActions = mockMvc
				.perform(put("/trade/{id}", arg1).contentType(MediaType.APPLICATION_JSON).content(asJsonString(trade)));
	}

	@Then("^the api should update specific trades and return code (\\d+) OK$")
	public void the_api_should_update_specific_trades_and_return_code_OK(int arg1) throws Throwable {
		resultActions.andExpect(status().isOk());
	}

	@When("^the client requests DELETE /trade/id with id \"([^\"]*)\"$")
	public void the_client_requests_DELETE_trade_id_with_id(String arg1) throws Throwable {
		resultActions = mockMvc.perform(delete("/trade/{id}", arg1).contentType(MediaType.APPLICATION_JSON));

	}

	@Then("^the api should update specific trades and return code (\\d+) NoContent$")
	public void the_api_should_update_specific_trades_and_return_code_NoContent(int arg1) throws Throwable {

		resultActions.andExpect(status().isNoContent());
	}

	@When("^the client requests DELETE /trade$")
	public void the_client_requests_DELETE_trade() throws Throwable {
		resultActions = mockMvc.perform(delete("/trade"));
	}

	@Then("^return code (\\d+) NoContent$")
	public void return_code_NoContent(int arg1) throws Throwable {
		resultActions.andExpect(status().isNoContent());
	}

	private static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private List<String> inputFormatterUtil(DataTable arg1) {

		List<List<String>> data = arg1.raw();
		List<String> dataList = new ArrayList<>();
		for (List<String> list : data) {
			for (String string : list) {
				if (string.equals("underLying")) {
				} else {
					dataList.add(string);

				}

			}
		}

		return dataList;
	}

	@After
	public void destroy() {
		tradeService.deletAlltrade();

	}

}
